import {createStore} from 'vuex';

export const store = createStore({
        state: {
            adminData: null
        },
        mutations: {
            setAdminData(state, payload) {
                state.adminData = payload
            },
        },
        actions: {
            setAdminData(context, payload) {
                context.commit('setAdminData', payload)
            },
        },
        getters: {
            getAdminData(state) {
                return state.adminData;
            },
        }
    }
);