import { createApp } from 'vue'
import App from './App.vue'

import routers from "./routes";
import {store} from "./store";

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

//icons
import {aliases, mdi} from 'vuetify/iconsets/mdi-svg'
import {
    mdiHome,
    mdiMagnify,
    mdiCalendarMultiselect,
    mdiAccountCreditCardOutline,
    mdiPencilOutline,
    mdiTrashCanOutline,
    mdiCellphone,
    mdiFaceWoman,
    mdiFaceMan,
    mdiCalendarStar,
    mdiEmailArrowRightOutline,
    mdiComment,
    mdiAccountPlus,
    mdiChevronDown,
    mdiChevronUp,
    mdiPlus,
    mdiBell,
    mdiInformation,
    mdiAccountEdit,
    mdiPhone,
    mdiRoomService
} from '@mdi/js'

const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi',
        aliases: {
            ...aliases,
            home: mdiHome,
            search: mdiMagnify,
            calendar: mdiCalendarMultiselect,
            subscription: mdiAccountCreditCardOutline,
            edit: mdiPencilOutline,
            delete: mdiTrashCanOutline,
            cellphone: mdiCellphone,
            woman: mdiFaceWoman,
            man: mdiFaceMan,
            birthday: mdiCalendarStar,
            mail: mdiEmailArrowRightOutline,
            message: mdiComment,
            user: mdiAccountPlus,
            down: mdiChevronDown,
            up: mdiChevronUp,
            add: mdiPlus,
            bell: mdiBell,
            info: mdiInformation,
            writeUser: mdiAccountEdit,
            phone: mdiPhone,
            service: mdiRoomService
        },
        sets: {
            mdi,
        },
    },
})
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'

const app = createApp(App);
app.use(routers)
app.use(store)
app.use(vuetify)

app.mount('#app')
