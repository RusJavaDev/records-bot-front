import {createRouter, createWebHistory} from "vue-router";
import HomePage from '@/pages/home.vue'
import CalendarPage from '@/pages/calendar.vue'
import DistributePage from '@/pages/distribute.vue'
import InformationPage from '@/pages/info.vue'
import FavorPage from '@/pages/favor.vue'

const routerHistory = createWebHistory();

const routers = createRouter({
    history: routerHistory,
    routes: [
        {
            path: '/admin/:hash',
            name: 'home',
            component: HomePage,
        },
        {
            path: '/admin/calendar/:hash',
            name: 'calendar',
            component: CalendarPage,
        },
        {
            path: '/admin/distribute/:hash',
            name: 'distribute',
            component: DistributePage,
        },
        {
            path: '/admin/info/:hash',
            name: 'info',
            component: InformationPage,
        },
        {
            path: '/admin/favor/:hash',
            name: 'favor',
            component: FavorPage,
        },
    ]
})

export default routers