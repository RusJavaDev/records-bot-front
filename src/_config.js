export const links = [
    {
        title: "Home",
        alias: "home",
        url: `/admin/`
    },
    {
        title: "Calendar",
        alias: "calendar",
        url: `/admin/calendar/`
    },
    {
        title: "Distribute",
        alias: "distribute",
        url: `/admin/distribute/`
    },
    {
        title: "Information",
        alias: "info",
        url: `/admin/info/`
    },
    {
        title: "Favor",
        alias: "favor",
        url: `/admin/favor/`
    },
]